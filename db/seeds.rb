# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Company.delete_all
Person.delete_all
(0..50).each do |i|
  Company.create("name" => Faker::Company.name, "country" => Faker::Address.country)
  (0..10).each do |j|
    Person.create("first_name" => Faker::Name.first_name, "last_name" => Faker::Name.last_name, "company_id" => i+1)
  end
end