class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.string :user
      t.string :company
      t.string :person

      t.timestamps null: false
    end
  end
end
