require 'test_helper'

class FavoritesTestTest < ActionDispatch::IntegrationTest
  get '/users/sign_in'
  assert_response :success

  post_via_redirect '/users/sign_in', [email= "kisiel12@op.pl", password= "qwertyui"]
  assert_equal '/company/index', path

  get '/company/index'
  assert_response :success
  post_via_redirect '/favorites/save', [company_id= "Kuhn and Sons"]

  get '/people/show?id=7'
  assert_response :success
  post_via_redirect '/favorites/save', [person_id= "Mariane, D'Amore"]

  get '/favorites'
  assert_select 'li.company', 'Kuhn and Sons, Western Sahara'
  assert_select 'li.person', 'Mariane, D\'Amore'
  assert_response :success
end
