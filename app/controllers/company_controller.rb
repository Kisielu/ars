class CompanyController < ApplicationController
  before_action :authenticate_user!
  def index
    @companies = Company.all
  end
  def find_by_index(id)
    @company = Company.find(id)
  end
end
