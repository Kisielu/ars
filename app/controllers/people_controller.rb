class PeopleController < ApplicationController
  before_action :authenticate_user!
  def show
    @people_on_companies = Person.where("company_id = ?", params[:id])
    company = CompanyController.new
    @company = company.find_by_index(params[:id])
  end
end
