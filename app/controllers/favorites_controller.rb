class FavoritesController < ApplicationController
  before_action :authenticate_user!
  def index
    @user = current_user.id.to_s

    @favorites_companies = Favorite.where("\"user\" = ? AND \"company\" IS NOT NULL", @user).order(created_at: :desc)
    @favorites_people = Favorite.where("\"user\" = ? AND \"person\" IS NOT NULL", @user).order(created_at: :desc)

    @people_list = []

    @favorites_people.each do |fav_ppl|
      @temp = Person.find(fav_ppl.id)
      @name = fav_ppl.person + ', '
      @temp_comp = Company.find(@temp.company_id)
      @corp_name = @temp_comp.name+ ' company'
      @people_list.push([@name, @corp_name])
    end

    @count = @favorites_companies.length + @favorites_people.length

  end
  def save
    @user = current_user.id.to_s
    @favorite = Favorite.create(user: @user, company: params[:company_id], person: params[:person_id])
  end
end
