class Favorite < ActiveRecord::Base
  validates :user, uniqueness: { scope: [:person, :company] }
end
